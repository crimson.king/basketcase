import unittest
from unittest.mock import MagicMock
from importlib import resources

import requests
import json

from basketcase.extractor import ProfileExtractor, PostExtractor, HighlightExtractor, Extractor, AbstractExtractor
from basketcase.models import ResourceImage, ResourceVideo


class ExtractorTest(unittest.TestCase):
    def test_extract_from_media_info_object(self):
        http_client = MagicMock(spec=requests.Session)

        request = MagicMock(spec=requests.Request)
        request.url = 'https://www.instagram.com/john.doe'
        response = MagicMock(spec=requests.Response)
        response.request = request

        extractor = ProfileExtractor(identifier='test', http_client=http_client, html_response=response)

        media_info = resources.files('tests.extractor.fixture').joinpath('profile.json').read_text()
        media_info = json.loads(media_info)

        # Pick two items to test, by numeric index
        image_index = 0  # This contains one image
        video_index = 8  # This contains one image and one video

        user = media_info['reels']['5453367478']['user']

        # Starting image test
        item = media_info['reels']['5453367478']['items'][image_index]
        downloads = extractor.extract_from_media_info_object(item=item, user=user)

        expected_image = ResourceImage(
            id='2683840030407476312_5453367478',
            url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                '/366178805_307780728320910_5495781189069325666_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1.fna'
                '.fbcdn.net&_nc_cat=106&_nc_ohc=MYBkFxkBEb0AX8t0No1&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                '=fHTgsJEUXD5s7RdvKFkyMjQ1Nw%3D%3D.2-ccb7-5&oh=00_AfCE1mO7LVQZl7svFx4tB6czfOSPu7IC-e_echl9ZyBXGA&oe'
                '=64D270EE&_nc_sid=982cc7',
            username='john.doe'
        )

        self.assertCountEqual(downloads, {expected_image})

        # Starting video test
        item = media_info['reels']['5453367478']['items'][video_index]
        downloads = extractor.extract_from_media_info_object(item=item, user=user)

        expected_image = ResourceImage(
            id='3163616268007468539_5453367478',
            url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                '/365771303_201873250514092_2947873579578494455_n.jpg?stp=dst-jpg_e15&_nc_ht=instagram.fcac3-1.fna'
                '.fbcdn.net&_nc_cat=108&_nc_ohc=PW3b2Tk7PbgAX-8fLnG&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                '=MzE2MzYxNjI2ODAwNzQ2ODUzOQ%3D%3D.2-ccb7-5&oh=00_AfAnyyq6RDeO4G7eHm1vq2aRzJqsMUOT2Ygb57pHrnvseQ&oe'
                '=64D2C1E4&_nc_sid=982cc7',
            username='john.doe'
        )
        expected_video = ResourceVideo(
            id='3163616268007468539_5453367478',
            url='https://instagram.fcac3-1.fna.fbcdn.net/v/t66.30100-16'
                '/319186706_247841508176650_8914544173385814053_n.mp4?efg'
                '=eyJ2ZW5jb2RlX3RhZyI6InZ0c192b2RfdXJsZ2VuLjEwODAuc3RvcnkuYmFzZWxpbmUiLCJxZV9ncm91cHMiOiJbXCJpZ193ZWJ'
                'fZGVsaXZlcnlfdnRzX290ZlwiXSJ9&_nc_ht=instagram.fcac3-1.fna.fbcdn.net&_nc_cat=110&_nc_ohc=ClNagcvyrZ8A'
                'X8SQt3I&edm=ANmP7GQBAAAA&vs=277239484937861_2538794782&_nc_vs=HBkcFQAYJEdCSm5CaE1LTXc0WmFlRUFBQ1dRTU1'
                'XeDByWjdicFIxQUFBRhUAAsgBACgAGAAbAYgHdXNlX29pbAExFQAAJt6oiOSE0PU%2FFQIoAkMzLBdANZDlYEGJNxgWZGFzaF9iYX'
                'NlbGluZV8xMDgwcF92MREAdegHAA%3D%3D&ccb=7-5&oh=00_AfAwfjEvGrM-eolhFiDQelO14cCW3IwCVrj3huPoZrTCOw&oe=64'
                'D27753&_nc_sid=982cc7',
            username='john.doe'
        )

        self.assertCountEqual(downloads, {expected_image, expected_video})

    def test_profile_extractor(self):
        json_data = resources.files('tests.extractor.fixture').joinpath('profile.json').read_text()
        json_data = json.loads(json_data)

        response = MagicMock(spec=requests.Response)
        response.json.return_value = json_data

        http_client = MagicMock(spec=requests.Session)
        http_client.get.return_value = response

        extractor = ProfileExtractor(identifier='5453367478', http_client=http_client, html_response=response)

        dummy_profile_picture = ResourceImage(
            url='dummy_profile_picture',
            id='dummy_profile_picture',
            username='dummy_profile_picture'
        )
        profile_picture_method = MagicMock()
        profile_picture_method.return_value = dummy_profile_picture
        extractor._get_profile_picture = profile_picture_method

        downloads = extractor.extract()

        expected_resources = {
            dummy_profile_picture,
            ResourceImage(
                id='2683840030407476312_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/366178805_307780728320910_5495781189069325666_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=106&_nc_ohc=MYBkFxkBEb0AX8t0No1&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=fHTgsJEUXD5s7RdvKFkyMjQ1Nw%3D%3D.2-ccb7-5&oh=00_AfCE1mO7LVQZl7svFx4tB6czfOSPu7IC-e_echl9ZyBXGA'
                    '&oe=64D270EE&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163550841956829913_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365310307_1343327219874640_4640289095165939767_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=104&_nc_ohc=qjPNNegwtyrzE--4QwZ&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzU1MDg0MTk1NjgyOTkxMw%3D%3D.2-ccb7-5&oh=00_AfC9pgUQ8MohxWlobT5_peySM6ItGo1dFhe8sF5dX2wveg'
                    '&oe=64D29CC9&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163552850969645839_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365438320_268906315871899_1971301581564563394_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=107&_nc_ohc=xEfOVx1UXi4Zs1X0qCQ&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzU1Mjg1MDk2OTY0NTgzOQ%3D%3D.2-ccb7-5&oh=00_AfCO65IlIkRCpgPC5GPGBksnN6afgj95xQtUM50Cv_n8YQ'
                    '&oe=64D2F9EB&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163572030010631368_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365388563_945876133160910_5853327462789528770_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=106&_nc_ohc=rafckGIJgpQAX9n2S0A&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzU3MjAzMDAxMDYzMTM2OA%3D%3D.2-ccb7-5&oh=00_AfCvFk6wwM80OD0MHiXRLJf_0JiMPuxKyURtx3G1qkLMZg'
                    '&oe=64D2E2E1&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163572319971210671_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/364389618_698206400186084_5977568567202934096_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=105&_nc_ohc=uTVidjvtzUoAX9r6_vW&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzU3MjMxOTk3MTIxMDY3MQ%3D%3D.2-ccb7-5&oh=00_AfD1dcgVlENB97WDQkDs93sd9YXmJP2TRUOmjH_H_75SWw'
                    '&oe=64D2B8F4&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163572418596298461_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/364948365_1642997882893211_8265870164382615732_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=111&_nc_ohc=Mgykthh4PRcAX_SYOqS&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzU3MjQxODU5NjI5ODQ2MQ%3D%3D.2-ccb7-5&oh=00_AfCLONPDJIO4vNhzs6QPIlUfU4r_NcTg17j1gwbrdkGOuQ'
                    '&oe=64D2EE48&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163615323643330608_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365405692_732279168661721_7323795077226298249_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=105&_nc_ohc=ZO7ouSqvcZQAX8v2HBM&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzYxNTMyMzY0MzMzMDYwOA%3D%3D.2-ccb7-5&oh=00_AfD3YYcypKMvA4rxX4o7sS6mtu7JhGjKgedmo5NzMrUJzg'
                    '&oe=64D29FE9&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163615551335378519_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/366010596_256227240509650_509073738634401074_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1.fna'
                    '.fbcdn.net&_nc_cat=110&_nc_ohc=WWDUG4N6QjsAX9vQ6PB&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzYxNTU1MTMzNTM3ODUxOQ%3D%3D.2-ccb7-5&oh=00_AfAJXakL5HXS8UoWSbv9HwnJm4h9si1Y2CsMpWkxIBOtXg'
                    '&oe=64D27CBD&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163616268007468539_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365771303_201873250514092_2947873579578494455_n.jpg?stp=dst-jpg_e15&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=108&_nc_ohc=PW3b2Tk7PbgAX-8fLnG&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzYxNjI2ODAwNzQ2ODUzOQ%3D%3D.2-ccb7-5&oh=00_AfAnyyq6RDeO4G7eHm1vq2aRzJqsMUOT2Ygb57pHrnvseQ'
                    '&oe=64D2C1E4&_nc_sid=982cc7'
            ),
            ResourceVideo(
                id='3163616268007468539_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t66.30100-16'
                    '/319186706_247841508176650_8914544173385814053_n.mp4?efg'
                    '=eyJ2ZW5jb2RlX3RhZyI6InZ0c192b2RfdXJsZ2VuLjEwODAuc3Rvcnku'
                    'YmFzZWxpbmUiLCJxZV9ncm91cHMiOiJbXCJpZ193ZWJfZGVsaXZlcnlfdn'
                    'RzX290ZlwiXSJ9&_nc_ht=instagram.fcac3-1.fna.fbcdn.net&_nc_cat'
                    '=110&_nc_ohc=ClNagcvyrZ8AX8SQt3I&edm=ANmP7GQBAAAA&vs=277239484'
                    '937861_2538794782&_nc_vs=HBkcFQAYJEdCSm5CaE1LTXc0WmFlRUFBQ1dRTU1'
                    'XeDByWjdicFIxQUFBRhUAAsgBACgAGAAbAYgHdXNlX29pbAExFQAAJt6oiOSE0PU%'
                    '2FFQIoAkMzLBdANZDlYEGJNxgWZGFzaF9iYXNlbGluZV8xMDgwcF92MREAdegHAA%3D'
                    '%3D&ccb=7-5&oh=00_AfAwfjEvGrM-eolhFiDQelO14cCW3IwCVrj3huPoZrTCOw&oe='
                    '64D27753&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163616361708445080_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365469556_305320505400053_4064471012702084356_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=105&_nc_ohc=dPexUSu4PRwAX8Nvdza&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzYxNjM2MTcwODQ0NTA4MA%3D%3D.2-ccb7-5&oh=00_AfC6JGzYd8fsz-15jscoAchNQZ6nJBmQ63Qe8PhZrg7GgQ'
                    '&oe=64D2CA8B&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163616367311975653_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365466710_1397696394121559_3132100388954798470_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=111&_nc_ohc=NO5kpLLOmnUAX-I-4uE&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=vuW66dTc0Ep8UphKMTk3NTY1Mw%3D%3D.2-ccb7-5&oh=00_AfA53LOWZ7W-H_J_FBrEOSquUDCT1hoSv7dDkSH3GwF2Og'
                    '&oe=64D301BD&_nc_sid=982cc7'
            ),
            ResourceImage(
                id='3163620390899308643_5453367478',
                username='john.doe',
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/365388134_1725904734573619_8263292525808920686_n.jpg?stp=dst-jpg_e35&_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net&_nc_cat=107&_nc_ohc=F85r5L5bqy8AX_xk1w_&edm=ANmP7GQBAAAA&ccb=7-5&ig_cache_key'
                    '=MzE2MzYyMDM5MDg5OTMwODY0Mw%3D%3D.2-ccb7-5&oh=00_AfDvEeU5Va97FRXoQDcM0xnOI2fN5tWATHxDHPeVAo5_7Q'
                    '&oe=64D271D3&_nc_sid=982cc7'
            )
        }

        self.assertCountEqual(downloads, expected_resources)

    def test_profile_extractor_get_profile_picture(self):
        # Load test data
        json_data = resources.files('tests.extractor.fixture').joinpath('user_info.json').read_text()
        json_data = json.loads(json_data)

        # Mock the http client objects and the json response
        response = MagicMock(spec=requests.Response)
        response.json.return_value = json_data

        # This method expects a "response.request.url", from which the username is extracted
        request = MagicMock(spec=requests.Request)
        request.url = 'https://www.instagram.com/jane.doe'
        response.request = request

        http_client = MagicMock(spec=requests.Session)
        # mocking return value of context manager requires using __enter__
        http_client.get.return_value.__enter__.return_value = response

        # Begin test
        extractor = ProfileExtractor(identifier='dummy', http_client=http_client, html_response=response)
        profile_image = extractor._get_profile_picture()

        expected_profile_image = ResourceImage(
            url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-19'
                '/324232747_6274417499248948_7760474050859390967_n.jpg?stp=dst-jpg_s320x320&_nc_ht=instagram.fcac3-1'
                '.fna.fbcdn.net&_nc_cat=1&_nc_ohc=y6VRxg_71oUAX9iLPMB&edm=AOQ1c0wBAAAA&ccb=7-5&oh'
                '=00_AfCWCt1ZFd8Z5Yf541HVmpFGFBeZdnGBBHHZfrBxfIXtuQ&oe=64DB2D08&_nc_sid=8b3546',
            id='1562931764',
            username='jane.doe'
        )

        self.assertEqual(profile_image, expected_profile_image)

    def test_post_extractor(self):
        # Load test data
        json_data = resources.files('tests.extractor.fixture').joinpath('post.json').read_text()
        json_data = json.loads(json_data)

        # Mock the http client and its json response
        response = MagicMock(spec=requests.Response)
        response.json.return_value = json_data

        mock_http = MagicMock(spec=requests.Session)
        mock_http.get.return_value = response

        # Begin test
        extractor = PostExtractor(identifier='66112036902686451940', http_client=mock_http, html_response=response)
        downloads = extractor.extract()

        expected_downloads = {
            ResourceImage(
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/000689579_577805180999616_2643185180353705472_n.jpg?stp=dst-jpg_e35\\u0026_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net\\u0026_nc_cat=100\\u0026_nc_ohc=USNB49tGAR4AX8hFSNh\\u0026edm=ALQROFkBAAAA\\u0026ccb'
                    '=7-5\\u0026ig_cache_key=MzA5OTY3Njg4MzkwNTQxMzk2Ng%3D%3D.2-ccb7-5\\u0026oh=00_AfB5kkHejGp84VgBU'
                    '-2ihfAWSDrR4DdxSky8j2oQzfOjCA\\u0026oe=64DD5760\\u0026_nc_sid=fc8dfb',
                username='username2',
                id='3099676883905413966_91432609745'
            ),
            ResourceImage(
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/346109479_724821729424944_8353113810765168764_n.jpg?stp=dst-jpg_e35\\u0026_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net\\u0026_nc_cat=100\\u0026_nc_ohc=xnTnhLobQXcAX_rib6w\\u0026edm=ALQROFkBAAAA\\u0026ccb'
                    '=7-5\\u0026ig_cache_key=MzA5OTY3Njg4Mzg4ODg4MDcwOA%3D%3D.2-ccb7-5\\u0026oh=00_AfCsfLzv4RD'
                    '-NDwN_c66iPRfpiDy-rBv-dRNgh8Pw6lPeQ\\u0026oe=64DE1217\\u0026_nc_sid=fc8dfb',
                username='username2',
                id='3099676883888880708_91432609745'
            ),
            ResourceImage(
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/345685055_1287489031842488_1346899183790222726_n.jpg?stp=dst-jpg_e35\\u0026_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net\\u0026_nc_cat=108\\u0026_nc_ohc=Z7hxxuwyWFIAX-09U7n\\u0026edm=ALQROFkBAAAA\\u0026ccb'
                    '=7-5\\u0026ig_cache_key=MzA5OTY3Njg4Mzg2MzU1MjIyNA%3D%3D.2-ccb7-5\\u0026oh'
                    '=00_AfBhwzyGvGO_3D09kGqQEtIukl7e6pNV1G63Uopwbt2_hw\\u0026oe=64DE2A35\\u0026_nc_sid=fc8dfb',
                username='username2',
                id='3099676883863552224_91432609745'
            ),
            ResourceImage(
                url='https://instagram.fcac3-1.fna.fbcdn.net/v/t51.2885-15'
                    '/345622143_566449732265917_7740494551694487718_n.jpg?stp=dst-jpg_e35\\u0026_nc_ht=instagram.fcac3-1'
                    '.fna.fbcdn.net\\u0026_nc_cat=101\\u0026_nc_ohc=HL_IjhwbTMkAX-TIhrW\\u0026edm=ALQROFkBAAAA\\u0026ccb'
                    '=7-5\\u0026ig_cache_key=MzA5OTY3Njg4MzkzMDc0MjMzNQ%3D%3D.2-ccb7-5\\u0026oh'
                    '=00_AfBEKlCfjBWuOsFMZ9WA1VepgEhxiVh1WwmVWo0nJ-jhnA\\u0026oe=64DE57EA\\u0026_nc_sid=fc8dfb',
                username='username2',
                id='3099676883930742335_91432609745'
            )
        }

        self.assertCountEqual(downloads, expected_downloads)

    def test_highlight_extractor(self):
        # Load test data
        json_data = resources.files('tests.extractor.fixture').joinpath('highlight.json').read_text()
        json_data = json.loads(json_data)

        # Mock the http client and its json response
        response = MagicMock(spec=requests.Response)
        response.json.return_value = json_data

        mock_http = MagicMock(spec=requests.Session)
        mock_http.get.return_value = response

        # Begin test
        extractor = HighlightExtractor(identifier='highlight:18332138656101156', http_client=mock_http, html_response=response)
        downloads = extractor.extract()

        expected_downloads = {
            ResourceImage(
                id='3258452573681308021_25226731096',
                username='flynoaa',
                url='https://instagram.fmgf14-1.fna.fbcdn.net/v/t39.30808-6'
                    '/410807263_18024547207843097_7633906546561875016_n.jpg?stp=dst-jpg_e35\u0026efg'
                    '=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xMTI1eDIwMDAuc2RyLmYzMDgwOCJ9\u0026_nc_ht=instagram'
                    '.fmgf14-1.fna.fbcdn.net\u0026_nc_cat=105\u0026_nc_ohc=8MjIaV4d0c8Q7kNvgGszLTI\u0026edm'
                    '=ANmP7GQAAAAA\u0026ccb=7-5\u0026ig_cache_key=MzI1ODQ1MjU3MzY4MTMwODAyMQ%3D%3D.2-ccb7-5\u0026oh'
                    '=00_AYDEfNSaRt72RVoLRbaFTSGcZKUI783C6Tl46Ay6PxO5XA\u0026oe=66A8AD9E\u0026_nc_sid=982cc7'
            )
        }

        self.assertCountEqual(downloads, expected_downloads)

    def test_extract_from_url(self):
        # Prepare mocks
        mock_response = MagicMock(spec=requests.Response)
        mock_response.text = ''
        mock_http_client = MagicMock(spec=requests.Session)
        mock_http_client.get.return_value = mock_response

        # Highlight test
        html_response_text = resources.files('tests.extractor.fixture').joinpath('highlight.html').read_text()
        mock_response.text = html_response_text
        basketcase_extractor = Extractor(http_client=mock_http_client)

        media_extractor = basketcase_extractor._get_extractor(
            'https://www.instagram.com/stories/highlights/18332138656101156/'
        )
        self.assertIsInstance(media_extractor, HighlightExtractor)
        self.assertEqual(media_extractor.id, '18332138656101156')

        # Profile test
        html_response_text = resources.files('tests.extractor.fixture').joinpath('profile.html').read_text()
        mock_response.text = html_response_text
        basketcase_extractor = Extractor(http_client=mock_http_client)

        media_extractor = basketcase_extractor._get_extractor('https://www.instagram.com/username1/')
        self.assertIsInstance(media_extractor, ProfileExtractor)
        self.assertEqual(media_extractor.id, '993248149')

        # Post test
        html_response_text = resources.files('tests.extractor.fixture').joinpath('post.html').read_text()
        mock_response.text = html_response_text
        basketcase_extractor = Extractor(http_client=mock_http_client)

        media_extractor = basketcase_extractor._get_extractor('https://www.instagram.com/p/CqeDC5ipXMo/')
        self.assertIsInstance(media_extractor, PostExtractor)
        self.assertEqual(media_extractor.id, '3070905331736688424')

    def test_find_best_size(self):
        mock_http = MagicMock(spec=requests.Session)
        response = MagicMock(spec=requests.Response)
        extractor = AbstractExtractor(identifier='dummy', http_client=mock_http, html_response=response)

        options = [
            {
                'width': 750,
                'height': 1333
            },
            {
                'width': 240,
                'height': 427
            },
            {
                'width': 1080,
                'height': 1080
            },
            {
                'width': 750,
                'height': 750
            },
            {
                'width': 150,
                'height': 150
            }
        ]

        # Test with a specified original size
        expected_option = options[0]
        selected_option = extractor.find_best_size(options, original_width=750, original_height=1333)
        self.assertDictEqual(selected_option, expected_option)

        # Test with a calculated best size
        expected_option = options[2]
        selected_option = extractor.find_best_size(options)
        self.assertDictEqual(selected_option, expected_option)


if __name__ == '__main__':
    unittest.main()

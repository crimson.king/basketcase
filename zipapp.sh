#!/bin/sh

# Cleanup
if [ -d ./zipapp ]; then
  rm -r ./zipapp
fi

if [ -f ./dist/basketcase ]; then
  rm ./dist/basketcase
fi

if [ ! -d ./dist ]; then
  mkdir ./dist
fi

# Begin
mkdir ./zipapp

echo 'Installing requirements'
pipenv run pip freeze --exclude-editable | pipenv run pip install --requirement /dev/stdin --target ./zipapp
echo 'Installing BasketCase'
pipenv run pip install . --target ./zipapp --upgrade

echo 'Generating zipapp'
python3 -m zipapp ./zipapp \
  --main="basketcase.__main__:main" \
  --python="/usr/bin/env python3" \
  --output=./dist/basketcase --compress

rm -r ./zipapp
